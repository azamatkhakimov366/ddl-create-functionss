-- Drop the existing function if it exists
DROP FUNCTION IF EXISTS new_movie(title_param VARCHAR);

-- Create a new function
CREATE OR REPLACE FUNCTION new_movie(title_param VARCHAR)
RETURNS VOID AS $$
DECLARE
    new_film_id INT;
BEGIN
    PERFORM language_id FROM language WHERE name = 'Klingon';
    IF NOT FOUND THEN
        INSERT INTO language (name) VALUES ('Klingon');
    END IF;

    SELECT COALESCE(MAX(film_id) + 1, 1) INTO new_film_id FROM film;

    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (new_film_id, title_param, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), 
            (SELECT language_id FROM language WHERE name = 'Klingon'));

    RAISE NOTICE 'New movie inserted with ID % and title %', new_film_id, title_param;
END;
$$ LANGUAGE plpgsql;

select new_movie('test movie');

select * from film fm where 
fm.title = 'test movie';
	